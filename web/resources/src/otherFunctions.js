(function ($) {
$(document).ready(function() {
	
	var text_max = 99;
    $('#characterCount').html(text_max + ' characters remaining');

    $('#message').keyup(function() {
        var text_length = $('#message').val().length;
        var text_remaining = text_max - text_length;

        $('#characterCount').html(text_remaining + ' characters remaining');
    });
	
	 });
}(jQuery));