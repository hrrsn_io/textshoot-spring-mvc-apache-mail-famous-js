<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <title>textShoot!</title>
        <meta name="viewport" content="width=device-width, maximum-scale=1, user-scalable=no" />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />

        <!-- shims for backwards compatibility -->
        <script type="text/javascript" src="http://code.famo.us/lib/functionPrototypeBind.js"></script>
        <script type="text/javascript" src="http://code.famo.us/lib/classList.js"></script>
        <script type="text/javascript" src="http://code.famo.us/lib/requestAnimationFrame.js"></script>
       

		<%-- <script type="text/javascript" src="<c:url value="/resources/ladda.min.js" />"></script> --%>
        

        <!-- module loader -->
        <script type="text/javascript" src="http://code.famo.us/lib/require.js"></script>
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        <!-- famous -->
        <link rel="stylesheet" type="text/css" href="http://code.famo.us/famous/0.3.0/famous.css" />
        <script type="text/javascript" src="http://code.famo.us/famous/0.3.0/famous.min.js"></script>

        <!-- app code -->
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/app.css" />" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/famous_styles.css" />" />
        <!-- <script type="text/javascript" href="/resources/src/main.js"></script> -->
        <script type="text/javascript">
            require.config({
                baseUrl: './resources/src/'
            });
            require(['main']);
        </script> 
         <script type="text/javascript">
            require.config({
                baseUrl: './resources/src/'
            });
            require(['otherFunctions']);
        </script> 
    </head>
    <body></body>
</html>

