package com.textshoot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MessageController {

    @Autowired
    @Qualifier("messageImpl")
    MessageImpl messageImpl;

    @RequestMapping("*")
    public String test() {
        return "index";
    }

    @RequestMapping(value = "ajaxsubmit", method = RequestMethod.POST)
    public
    @ResponseBody
    String ajaxSubmit(@RequestParam("recipient") String recipient, @RequestParam("content") String content) {
        System.out.println("in ajaxSubmit");
        Message newMessage = new Message();

        newMessage.setMessageContent(content);
        newMessage.setMessageRecipient(recipient);

        //log to database
        messageImpl.insert(newMessage);

        //send the message
        SendApacheMail apacheMail = new SendApacheMail();

        try {
            apacheMail.send(content, recipient);
        } catch (Exception e) {
            e.printStackTrace();
            return "fail";
        }

        return "success";

    }

}