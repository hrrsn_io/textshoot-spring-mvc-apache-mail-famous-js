package com.textshoot;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.SimpleEmail;

public class SendApacheMail {

    public void send(String content, String recipient) throws Exception {

        SimpleEmail email = new SimpleEmail();
        email.setHostName("mail.hostname.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator("email@email.com", "password"));
        email.setSSLOnConnect(true);
        email.setFrom("shot@textshoot.com");
        email.addTo(recipient);
        email.setSubject("textShoot");
        email.setMsg(content);
        email.send();


    }
}