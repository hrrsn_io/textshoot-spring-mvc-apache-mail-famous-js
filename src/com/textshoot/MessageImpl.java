package com.textshoot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("messageImpl")
public class MessageImpl implements MessageDAO {

    @Autowired
    JdbcTemplate jdbctemplate;

    @Override
    public void insert(Message message) {

        String sql = "INSERT INTO message (content, recipient) VALUES (?, ?)";

        jdbctemplate.update(sql, message.getMessageContent(), message.getMessageRecipient());

    }

}
