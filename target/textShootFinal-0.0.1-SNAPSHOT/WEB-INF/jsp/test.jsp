<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title>Polititroll</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<%-- <link type="text/css" rel="stylesheet" href="<c:url value="/resources/ladda.min.css" />" /> --%>
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/ladda-themeless.min.css" />" />
<script type="text/javascript" src="<c:url value="/resources/spin.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/ladda.min.js" />"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script>
 $(document).ready(function(){
	 var category;
	 var carrier;
	 $( "#response" ).hide();
	 var l = Ladda.create( document.querySelector( '#submit' ) );
	 $("#dropdown1").children("ul li").click(function(){
		 console.log($("#categorySelection").html());
		 category = $(this).text();
		 $("#categorySelection").html(category+' <span class="caret"></span>'); 
	 	
	 });
	 
	 $("#dropdown2").children("ul li").click(function(){
		 console.log($("#carrierSelection").html());
		 carrier = $(this).text();
		 console.log(carrier);
		 $("#carrierSelection").html(carrier+' <span class="caret"></span>'); 
	 	
	 });
  
	 $("#submit").click(function(e){
	    $( "#response" ).hide();
	    e.preventDefault();
	    
	 	l.start();
	    
	  	console.log("button for submit clicked");
	  	var recipient = $("#phoneNumber").val();
        //var category = $('#category').val();
  	var intervalId = 0;
    intervalId = setInterval(ajaxCheck, 100);
     $.ajax({
  		
  		type : "post",
  		url : "ajaxsubmit",
  		data : {
					recipient : recipient,
					category : category,
					carrier : carrier
				},
  		success : function(data) {
  			clearInterval(intervalId);
  			l.stop();
  			if (data != null){
  			$('#response').append(data);
  			}
        }
  	
      }); 
  	
    });
	 var prevData = null;
	 function ajaxCheck(){
	        $.ajax({
	            url : 'ajaxcheck',
	            success : function(data) {
	            	
	            	if (data != null && data != prevData && prevData != null){
	            	$('#response').append(data+'<br>');
	            	$( "#response" ).show();
	            	}
	            	prevData = data;
	           }
	        }); 
	     }
	 
});

(jQuery);</script>

  

</head>


<body>
	<br>
	<!-- <div align='center'>
	</div>
Your Message:<br>
<form id="messageForm">
<input id="textBox" type="text" name="messageContent"> 
<br>Recipient:<br>
<input id="recipient" type="text" name="recipient">
</form>
<br><br>
<button id="submit">Submit</button> -->
<div class="container">
  <div class="jumbotron">
  <h2>Polititroll Load Test App</h2>
<form class="form-horizontal" role="form">
  <div class="form-group">
    <div class="col-sm-4">
      <input type="tel" class="form-control" id="phoneNumber" placeholder="Enter phone number">
    </div>
  </div>
  <div class="form-group">
   <div class="dropdown col-sm-4">
   <button id="categorySelection" class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    Category
    <span class="caret"></span>
  </button>
  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" id="dropdown1">
    <li><a href="#">Trump</a></li>
    <li><a href="#">Hillary</a></li>
    <li><a href="#">Bush</a></li>
    <!-- <li><a href="#">Random</a></li> -->
  </ul> 
  	
	</div>
	</div>
	<div class="form-group">
	<div class="dropdown col-sm-4">
   <button id="carrierSelection" class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    Carrier
    <span class="caret"></span>
  </button>
  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" id="dropdown2">
    <li><a href="#">Verizon</a></li>
    <li><a href="#">At&t</a></li>
    <li><a href="#">Tmobile</a></li>
    <li><a href="#">Sprint</a></li>
    <!-- <li><a href="#">Random</a></li> -->
  </ul> 
	</div>
	</div>
  <div class="form-group"> 
    <div class="col-sm-offset-0 col-sm-4">
      <button id="submit" class="btn btn-primary ladda-button" data-style="zoom-in">Submit</button>
    </div>
  </div>
</form>
<div id="response"class="alert alert-success">
</div>
</div>
</div>
</body>
</html>