/**
 * Transitionable
 * --------
 *
 * Transitionable is  state maintainer for a smooth transition between
 * numerically-specified states. Example numeric states include floats or
 * Matrix objects. Transitionables form the basis
 * of Transform objects.
 * 
 *  document.onmousemove = function(e){
	var x = e.pageX;
	var y = e.pageY;
	e.target.title = "X is "+x+" and Y is "+y;	
	};
 * 
 */
(function ($) {
$(document).ready(function() {
	
		var widthX = $( window ).width();
		var centerX =  (widthX / 2) - 150;
		
		var heightY = $( window ).height();
		var centerY = (heightY / 2) - 150;
		
		var startX = 50;
		var startY = 50;
	
	
	function windowSize() {
			console.log("X " +$( window ).width());
			console.log("Y " +$( window ).height());
			}
	windowSize();
	
	define(function(require, exports, module) {
	    var Engine         = require("famous/core/Engine");
	    var Surface        = require("famous/core/Surface");
	    var ContainerSurface = require("famous/surfaces/ContainerSurface");
	    var ImageSurface = require("famous/surfaces/ImageSurface");
	    var Modifier       = require("famous/core/Modifier");
	    var StateModifier = require('famous/modifiers/StateModifier');
	    var Transform      = require("famous/core/Transform");
	    var TransitionableTransform = require("famous/transitions/TransitionableTransform");
	    var Easing = require('famous/transitions/Easing');
	    var Flipper    = require("famous/views/Flipper");
	    var GridLayout = require("famous/views/GridLayout");
	
	    // create the main context
	    var mainContext = Engine.createContext();
	   
        var nahShootState = new StateModifier({
        	  size: [400, 100],
	    	  transform: Transform.translate(centerX +50, centerY +250, 0)
	    	});
	    
	    var grid = new GridLayout({
	        dimensions: [2, 1],
	        size: [400, 100],
	    });

	    var surfaces = [];
	    grid.sequenceFrom(surfaces);

	        surfaces.push(new Surface({
	            content: "nah",
	            size: [50, 50],
	            properties: {
	                backgroundColor: "#FF9900",
	                color: "black",
	                lineHeight: '50px',
	                textAlign: 'center'
	            },
	            attributes: {
		    		id: 'nah',
	            }
	        }));
	        
	        grid.sequenceFrom(surfaces);

	       
	        surfaces.push(new Surface({
	            content: "shoot!",
	            size: [50, 50],
	            properties: {
	                backgroundColor: "#669900",
	                color: "black",
	                lineHeight: '50px',
	                textAlign: 'center'
	            },
	            attributes: {
		    		id: 'shoot',
	            }
	        }));
	    
	        mainContext.add(nahShootState).add(grid);
	        
	        $('#nah').hide();
	        $('#shoot').hide();
	        
	        
	    
	     // initial state of buttonSurface on load
	    var initialState = new StateModifier({
	    	  transform: Transform.translate(100, 100, 0)
	    	});
	   
	    
	    // initial state of buttonSurface on load
	    var logoState = new StateModifier({
	    	  transform: Transform.translate(widthX - (widthX -25), heightY - 150, 0)
	    	});
	    
	    var logoContainer = new ContainerSurface({
	        size: [400, 400],
	    });
	    
	    
	    
	    
	    var flipper = new Flipper();
	    // logo surface
	    var logo = new ImageSurface({
	        size: [685, 135]
	    });
	    
	    var logoBack = new ImageSurface({
	        size: [685, 135]
	    });
	    
	    flipper.setFront(logo);
	    flipper.setBack(logoBack);

	    logo.setContent("img/logo.png");
	    logoBack.setContent("img/backLogo.png");

	    logoContainer.add(flipper);
	    
	    mainContext.add(logoState).add(logoContainer);
	    
	    var toggle = false;
	    logoContainer.on('click', function(){
	        var angle = toggle ? 0 : Math.PI;
	        flipper.setAngle(angle, {curve : 'easeOutBounce', duration : 1000});
	        toggle = !toggle;
	    });
	    
	    var surface = new Surface({
	        size:[100,100],
	        content: 'New',
	        classes: ['red-bg'],
	        properties: {
	            textAlign: 'center',
	            lineHeight: '100px',
	        },
	    	attributes: {
	    		id: 'surface',
	    		name: 'newLeft',
	    	},
	    });
	    
	    var surfaceMiddle = new Surface({
	        size:[400,400],
	        //content: 'New',
	        classes: ['red-bg'],
	        content: '  <form id="messageForm"> \
				        <br><textarea id="textBox" name="messageContent" rows="4" cols="20">Enter your text</textarea>  \
				        <br><input id="recipient" type="text" name="recipient" placeholder="Recipient"> \
				        </form>',
				        
	        properties: {
	            textAlign: 'center',
	            lineHeight: '100px',
	        },
	    	attributes: {
	    		id: 'formSurface',
	    		name: 'newLeft',
	    	},
	    });
	    
	    var nawState = new StateModifier({
	    	  transform: Transform.translate(widthX - (widthX / 3), heightY - (heightY - 2))
	    	});
	    
	    
	    /*<br> \
    	<button id="cancel">nah</button>  <button id="submit">shoot!</button> \
		        <div id="response"></div>',
*/	    
	    var transitionableTransform = new TransitionableTransform();
	    
	    var modifier = new Modifier({
	        //align: [.5, .5],
	        //origin: [.5, .5],
	        transform: transitionableTransform
	    });
	    
	    var stateModifier = new StateModifier();
	    var stateMiddleModifier = new StateModifier();
	    stateMiddleModifier.setTransform(
		          //Transform.translate(centerX, centerY, 0),
		          Transform.translate(centerX - 50, centerY - 200, 0)
		          //{ duration : 1200, curve: Easing.outElastic }
		          //mainContext.add(surfaceMiddle)
		          /*function() {
		            surface.setContent('content');
		          }*/
		        );
	    
	    // adding surface and modifiers to context
	    mainContext.add(stateModifier).add(initialState).add(modifier).add(surface);
	    
	    
	    function firstAnimation(){
	        stateModifier.setTransform(
	          //Transform.translate(centerX, centerY, 0),
	          Transform.translate(centerX - 150, centerY - 300, 0),
	          { duration : 1200, curve: Easing.outElastic }
	          //mainContext.add(surfaceMiddle)
	          /*function() {
	            surface.setContent('content');
	          }*/
	        );
	        }
	    
	    function secondAnimation() {
	    stateModifier.setTransform(
	    		Transform.translate(centerX * 2, 0, 0),
	            { duration : 1200, curve: Easing.outElastic }
	            /*function() {
	              surface.setContent('New');
	            }*/
	    );
	    }
	    
	    function thirdAnimation() {
		    stateModifier.setTransform(
		    		Transform.translate(0, 0, 0),
		            { duration : 1200, curve: Easing.outElastic }
		            /*function() {
		              surface.setContent('New');
		            }*/
		    );
		    }
	    
	    function hideSurface() {
	    	$('#formSurface').hide();
	    }
	    function showSurface() {
	    	$('#formSurface').show();
	    }
	    function delayHide() {
	    	setTimeout(hideSurface, 1200);
	    }
	    function delayShow() {
	    	setTimeout(showSurface, 1200);
	    }
	    
	    // click events
	    surface.on("click", function(){
	    	console.log("SURFACE " +surface.getSize());
	    	
	    	// first click left
	    	if ($('#surface').attr('name') === 'newLeft'){
	    	surface.setContent('');	
    		transitionableTransform.setScale([4,4,1], {duration: 400});
    		firstAnimation();
	    	delayHide();
	    	setTimeout(function(){mainContext.add(stateMiddleModifier).add(surfaceMiddle);}, 1200);
	    	surfaceMiddle.setAttributes({
	    		name: 'middleForm',
	    	});
	    	}
	    	
	    	// third click right
    		else if ($('#surface').attr('name') === 'newRight'){
    			surface.setContent('');
    			transitionableTransform.setScale([4,4,1], {duration: 400});
    			firstAnimation();
    			delayShow();
	    		surfaceMiddle.setAttributes({
    	    		name: 'middleFormReset',
    	    	});
	    	}
	    	
	    	// fifth click left
    		else if ($('#surface').attr('name') === 'resetLeft'){
    			surface.setContent('');
    			transitionableTransform.setScale([4,4,1], {duration: 400});
        		firstAnimation();
    	    	delayHide();
    	    	delayShow();
    	    	surfaceMiddle.setAttributes({
    	    		name: 'middleForm',
    	    	});
	    	}
	    });
	    
	    surfaceMiddle.on("click", function(){
	    	
	    	// second click middle
	    	if ($('#formSurface').attr('name') === 'middleForm'){
		    	$('#formSurface').hide();
		    	transitionableTransform.setScale([1,1,1], {duration: 400});
				surface.setContent('New');
		    	secondAnimation();
				surface.setAttributes({
		    		name: 'newRight',
		    	});
	    	}
	    	
	    	// fourth click middle
	    	else if ($('#formSurface').attr('name') === 'middleFormReset'){
	    		transitionableTransform.setScale([1,1,1], {duration: 400});
	    		surface.setContent('New');
	    		thirdAnimation();
	    		$('#formSurface').hide();
	    		surface.setAttributes({
    	    		name: 'resetLeft',
    	    	});
	    	}
	    });
	    
	    surfaces.on("click", function(){
	    	console.log('nah click');
	    	// fourth click middle
	    		transitionableTransform.setScale([1,1,1], {duration: 400});
	    		surface.setContent('New');
	    		thirdAnimation();
	    		$('#formSurface').hide();
	    		surface.setAttributes({
    	    		name: 'resetLeft',
    	    	});
	    });
	    
		});
	 });
}(jQuery));