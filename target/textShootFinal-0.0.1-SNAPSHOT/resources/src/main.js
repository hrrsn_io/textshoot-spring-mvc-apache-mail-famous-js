(function ($) {
$(document).ready(function() {
	
		var widthX = $( window ).width();
		var centerX =  (widthX / 2) - 150;
		
		var heightY = $( window ).height();
		var centerY = (heightY / 2) - 150;
		
		var startX = 50;
		var startY = 50;
	
	
	function windowSize() {
			console.log("X " +$( window ).width());
			console.log("Y " +$( window ).height());
			}
	windowSize();
	
	define(function(require, exports, module) {
	    var Engine         = require("famous/core/Engine");
	    var Surface        = require("famous/core/Surface");
	    var ContainerSurface = require("famous/surfaces/ContainerSurface");
	    var ImageSurface = require("famous/surfaces/ImageSurface");
	    var Modifier       = require("famous/core/Modifier");
	    var StateModifier = require('famous/modifiers/StateModifier');
	    var Transform      = require("famous/core/Transform");
	    var TransitionableTransform = require("famous/transitions/TransitionableTransform");
	    var Easing = require('famous/transitions/Easing');
	    var Flipper    = require("famous/views/Flipper");
	
	    // create the main context
	    var mainContext = Engine.createContext();
	    
	    // initial state of buttonSurface on load
	    var initialState = new StateModifier({
	    	  transform: Transform.translate(100, 100, 0)
	    	});
	   
	    
	    // initial state of buttonSurface on load
	    var logoState = new StateModifier({
	    	  transform: Transform.translate(widthX - (widthX -25), heightY - 150, 0)
	    	});
	    
	    var logoContainer = new ContainerSurface({
	        size: [400, 400],
	    });
	    
	    
	    
	    
	    var flipper = new Flipper();
	    // logo surface
	    var logo = new ImageSurface({
	        size: [685, 135]
	    });
	    
	    var logoBack = new ImageSurface({
	        size: [685, 135]
	    });
	    
	    flipper.setFront(logo);
	    flipper.setBack(logoBack);

	    logo.setContent("resources/img/logo.png");
	    logoBack.setContent("resources/img/backLogo.png");

	    logoContainer.add(flipper);
	    
	    mainContext.add(logoState).add(logoContainer);
	    
	    var toggle = false;
	    logoContainer.on('click', function(){
	        var angle = toggle ? 0 : Math.PI;
	        flipper.setAngle(angle, {curve : 'easeOutBounce', duration : 1000});
	        toggle = !toggle;
	    });
	    
	    var surface = new Surface({
	        size:[100,100],
	        content: 'New',
	        classes: ['red-bg'],
	        properties: {
	            textAlign: 'center',
	            lineHeight: '100px',
	        },
	    	attributes: {
	    		id: 'surface',
	    		name: 'newLeft',
	    	},
	    });
	    
	    var surfaceMiddle = new Surface({
	        size:[400,400],
	        //content: 'New',
	        classes: ['red-bg', 'hidden'],
	        content: '<br><textarea maxlength="500" placeholder="paste or type text to send to yourself!" id="message" name="messageContent" rows="4" cols="20"></textarea> \
	        		  <br><input id="recipient" type="text" name="recipient" placeholder="email">',
				        
	        properties: {
	            textAlign: 'center',
	            lineHeight: '50px',
	        },
	    	attributes: {
	    		id: 'formSurface',
	    		name: 'newLeft',
	    	},
	    });
	    
	    function isValidEmailAddress(emailAddress) {
	    	console.log('in validate' +emailAddress);
	    	var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
	    	if (emailAddress == '' || !re.test(emailAddress))
	    	{
	    	    alert('Please enter a valid email address.');
	    	    return false;
	    	} else {return true;}
	    };
	    
	    function ajax(){
	    	console.log("button for submit clicked");
		  	var message = $("#message").val();
	        var recipient = $("#recipient").val();
	  	$.ajax({
	  		
	  		type : "post",
	  		url : "ajaxsubmit",
	  		data : {
						content : message,
						recipient : recipient
					},
	  		success : function(data) {
	            console.log(data);
	        }
	  	
	      });   
	    }
	    
	    var nawState = new StateModifier({
	    	  transform: Transform.translate(centerX-50, centerY +200)
	    	});
	    
	    var shootState = new StateModifier({
	    	  transform: Transform.translate(centerX+150, centerY +200)
	    	});
	    
	    var nahSurface = new Surface({
	        size:[200,40],
	        classes: ['orange-bg', 'hidden'],
	        content: 'nah',
	        properties: {
	            textAlign: 'center',
	            lineHeight: '40px',
	        },
	    	attributes: {
	    		id: 'nahSurface',
	    		name: 'nahSurface',
	    	},
	    });
	    
	    var shootSurface = new Surface({
	        size:[200,40],
	        classes: ['green-bg', 'hidden'],
	        content: 'shoot!',
	        properties: {
	            textAlign: 'center',
	            lineHeight: '40px',
	        },
	    	attributes: {
	    		id: 'shootSurface',
	    		name: 'shootSurface',
	    	},
	    });
	    
	    
	    /*<br> \
    	<button id="cancel">nah</button>  <button id="submit">shoot!</button> \
		        <div id="response"></div>',
*/	    
	    var transitionableTransform = new TransitionableTransform();
	    
	    var modifier = new Modifier({
	        //align: [.5, .5],
	        //origin: [.5, .5],
	        transform: transitionableTransform
	    });
	    
	    var stateModifier = new StateModifier();
	    var stateMiddleModifier = new StateModifier();
	    stateMiddleModifier.setTransform(
		          //Transform.translate(centerX, centerY, 0),
		          Transform.translate(centerX - 50, centerY - 200, 0)
		          //{ duration : 1200, curve: Easing.outElastic }
		          //mainContext.add(surfaceMiddle)
		          /*function() {
		            surface.setContent('content');
		          }*/
	   	        );
	    // adding surface and modifiers to context
	    mainContext.add(stateModifier).add(initialState).add(modifier).add(surface);
	    mainContext.add(stateMiddleModifier).add(surfaceMiddle);
	    mainContext.add(nawState).add(nahSurface);
	    mainContext.add(shootState).add(shootSurface);
	    
	    document.addEventListener("DOMContentLoaded", function() {
	    	  //hideAll();
	    	});
	    
	    function firstAnimation(){
	        stateModifier.setTransform(
	          //Transform.translate(centerX, centerY, 0),
	          Transform.translate(centerX - 150, centerY - 300, 0),
	          { duration : 1200, curve: Easing.outElastic }
	          //mainContext.add(surfaceMiddle)
	          /*function() {
	            surface.setContent('content');
	          }*/
	        );
	        }
	    
	    function secondAnimation() {
	    stateModifier.setTransform(
	    		Transform.translate(centerX * 2, 0, 0),
	            { duration : 1200, curve: Easing.outElastic }
	            /*function() {
	              surface.setContent('New');
	            }*/
	    );
	    }
	    
	    function thirdAnimation() {
		    stateModifier.setTransform(
		    		Transform.translate(0, 0, 0),
		            { duration : 1200, curve: Easing.outElastic }
		            /*function() {
		              surface.setContent('New');
		            }*/
		    );
		    }
	    
	    function hideForm() {
	    	$('#formSurface').hide();
	    }
	    function showForm() {
	    	$('#formSurface').fadeIn(250);
	    }
	    function showButtons() {
	    	$('#shootSurface').fadeIn(250);
	    	$('#nahSurface').fadeIn(250);
	    }
	    function delayHide() {
	    	setTimeout(hideForm, 1000);
	    }
	    function delayShow() {
	    	setTimeout(showForm, 1000);
	    	setTimeout(showButtons, 1000);
	    }
	    
	    // click events
	    surface.on("click", function(){
	    	console.log("SURFACE " +surface.getSize());
	    	
	    	// first click left
	    	if ($('#surface').attr('name') === 'newLeft'){
	    	surface.setContent('');	
    		transitionableTransform.setScale([4,4,1], {duration: 400});
    		firstAnimation();
	    	//delayHide();
	    	//setTimeout(function(){mainContext.add(stateMiddleModifier).add(surfaceMiddle);}, 1200);
	    	//setTimeout(function(){mainContext.add(nawState).add(nahSurface);}, 1200);
	    	//setTimeout(function(){mainContext.add(shootState).add(shootSurface);}, 1200);
	    	//hideForm();
	    	//setTimeout(function(){$('#formSurface').hide();}, 1205);
	    	delayShow();
	    	//$('#nahSurface').hide();
	    	surfaceMiddle.setAttributes({
	    		name: 'middleForm',
	    	});
	    	}
	    	
	    	// third click right
    		else if ($('#surface').attr('name') === 'newRight'){
    			surface.setContent('');
    			transitionableTransform.setScale([4,4,1], {duration: 400});
    			firstAnimation();
    			delayShow();
	    		surfaceMiddle.setAttributes({
    	    		name: 'middleFormReset',
    	    	});
	    	}
	    	
	    	// fifth click left
    		else if ($('#surface').attr('name') === 'resetLeft'){
    			surface.setContent('');
    			transitionableTransform.setScale([4,4,1], {duration: 400});
        		firstAnimation();
        		delayHide();
    	    	delayShow();
    	    	surfaceMiddle.setAttributes({
    	    		name: 'middleForm',
    	    	});
	    	}
	    });
	    
	    shootSurface.on("click", function(){
	    	console.log('click on shoot' +$('#recipient').val());
	    	isValidEmailAddress($('#recipient').val());
	    	if (isValidEmailAddress($('#recipient').val()) == true){
	    		ajax();
	    		$('#shootSurface').hide();
		    	$('#nahSurface').hide();
	    	// second click middle
	    	if ($('#formSurface').attr('name') === 'middleForm'){
	    		console.log('in if');
		    	$('#formSurface').hide();
		    	transitionableTransform.setScale([1,1,1], {duration: 400});
				surface.setContent('New');
		    	secondAnimation();
				surface.setAttributes({
		    		name: 'newRight',
		    	});
	    	}
	    	
	    	// fourth click middle
	    	else if ($('#formSurface').attr('name') === 'middleFormReset'){
	    		transitionableTransform.setScale([1,1,1], {duration: 400});
	    		surface.setContent('New');
	    		thirdAnimation();
	    		$('#formSurface').hide();
	    		surface.setAttributes({
    	    		name: 'resetLeft',
    	    	});
	    	}
	    	}
	    });
	    
	    nahSurface.on("click", function(){
	    	console.log('click on nah');
	    	$('#shootSurface').hide();
	    	$('#nahSurface').hide();
	    	
	    	transitionableTransform.setScale([1,1,1], {duration: 400});
    		surface.setContent('New');
    		thirdAnimation();
    		$('#formSurface').hide();
    		surface.setAttributes({
	    		name: 'resetLeft',
	    	});
	    });
 
		});
	 });
}(jQuery));